#!/usr/bin/env python

#
# Standard geni-lib/portal libraries
#
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab as elab
import geni.rspec.igext as IG
import geni.urn as URN


tourDescription = """
Use this profile to instantiate an experiment to realize an end-to-end SDR-based mobile network.
These are the possible options for each component.

UE : srsLTE UE or Nexus 5

eNB : srsLTE eNB

Core network : Open Air Interface core network or srsLTE core network
""";

tourInstructions = """
Please refer to README in https://gitlab.flux.utah.edu/junguk/srsLTE-OAI.git 
""";


#
# PhantomNet extensions.
#
import geni.rspec.emulab.pnext as PN

#
# Globals
#
class GLOBALS(object):
    OAI_DS = "urn:publicid:IDN+emulab.net:phantomnet+ltdataset+oai-develop"
    OAI_SIM_DS = "urn:publicid:IDN+emulab.net:phantomnet+dataset+PhantomNet:oai"
#    SRSLTE="urn:publicid:IDN+emulab.net+image+PhantomNet:openlte-0-20-4Scan"
    SRSLTE="urn:publicid:IDN+emulab.net+image+PhantomNet:srsLTE"
    UE_IMG  = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:ANDROID444-STD")
    #urn:publicid:IDN+emulab.net+image+PhantomNet:srsEPC-OAICN
    OAI_EPC_IMG = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:srsEPC-OAICN")
    ADB_IMG = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:UBUNTU14-64-PNTOOLS")
    UE_HWTYPE = "nexus5"
    OAI_CONF_SCRIPT = "/usr/bin/sudo /local/repository/bin/config_oai.pl"

def connectOAI_DS(node, sim):
    # Create remote read-write clone dataset object bound to OAI dataset
    bs = request.RemoteBlockstore("ds-%s" % node.name, "/opt/oai")
    if sim == 1:
	bs.dataset = GLOBALS.OAI_SIM_DS
    else:
	bs.dataset = GLOBALS.OAI_DS
    bs.rwclone = True

    # Create link from node to OAI dataset rw clone
    node_if = node.addInterface("dsif_%s" % node.name)
    bslink = request.Link("dslink_%s" % node.name)
    bslink.addInterface(node_if)
    bslink.addInterface(bs.interface)
    bslink.vlan_tagging = True
    bslink.best_effort = True

#
# This geni-lib script is designed to run in the PhantomNet Portal.
#
pc = portal.Context()

#
# Profile parameters.
#
#pc.defineParameter("RADIATEDRF", "Radiated (over-the-air) RF transmissions",
#                   portal.ParameterType.BOOLEAN, False,
#                   longDescription="When enabled, RF devices with real antennas and transmissions propagated through free space will be selected.  Leave disabled (default) to assign RF devices connected via transmission lines with variable attenuator control.")

pc.defineParameter("TYPE", "UE type",
                   portal.ParameterType.STRING,"nexus5",[("srsue","SRS UE"),("nexus5","Nexus 5")],
                   longDescription="Type of UEs connected to eNB")


pc.defineParameter("NUMCLI", "Number of UEs",
                   portal.ParameterType.INTEGER, 1,
                   longDescription="Specify the number of User Equipments (UEs) resources to allocate. This number must be between 1 and 2 currently.",
                   advanced=True)

params = pc.bindParameters()

#
# Give the library a chance to return nice JSON-formatted exception(s) and/or
# warnings; this might sys.exit().
#
pc.verifyParameters()

#
# Create our in-memory model of the RSpec -- the resources we're going
# to request in our experiment, and their configuration.
#
request = pc.makeRequestRSpec()
epclink = request.Link("s1-lan")


# add sdr enb
sdrenb = request.RawPC( "sdrenb" )
sdrenb.hardware_type = "nuc5300"
sdrenb.disk_image = GLOBALS.SRSLTE
epclink.addNode(sdrenb)

if params.TYPE == "nexus5":
    adb_t = request.RawPC("adb-tgt")
    adb_t.disk_image = GLOBALS.ADB_IMG

for i in range(1, params.NUMCLI + 1):
    uename = "ue%d" % i
    ueint = "ueint%d" % i
    enbint = "renb%d" % i
    sdrenbif1 = sdrenb.addInterface( enbint )
    if params.TYPE == "srsue":
        ue = request.RawPC( uename )
        ue.hardware_type = "nuc5300"
        ue.disk_image = GLOBALS.SRSLTE
        ueif1 = ue.addInterface( ueint )
        #sdrueif2 = sdrue.addInterface( "rue2" )
    else:
        ue = request.UE( uename )
        ue.hardware_type = GLOBALS.UE_HWTYPE
        ue.disk_image = GLOBALS.UE_IMG
        ue.adb_target = "adb-tgt"
        ueif1 = ue.addInterface( ueint )

    rflname = "rflink%d" % i
    rflink1 = request.RFLink(rflname)
    rflink1.addInterface(sdrenbif1)
    rflink1.addInterface(ueif1)


#    cname = "client%d" % i
#    client = PN.mkepcnode(cname, PN.EPCROLES.CLIENT, hname = cname, request = request)
#    mgmt.addMember(client)
#    an_lte.addMember(client)



server = request.RawPC( "server" )
server.hardware_type = "d430"
server.disk_image = GLOBALS.SRSLTE
epclink.addNode(server)


# Add OAI EPC (HSS, MME, SPGW) node.
epc = request.RawPC("epc")
epc.disk_image = GLOBALS.OAI_EPC_IMG
epc.addService(rspec.Execute(shell="sh", command=GLOBALS.OAI_CONF_SCRIPT + " -r EPC"))
connectOAI_DS(epc, 0)
 
epclink.addNode(epc)
epclink.link_multiplexing = True
epclink.vlan_tagging = True
epclink.best_effort = True

tour = IG.Tour()
tour.Description(IG.Tour.MARKDOWN, tourDescription)
tour.Instructions(IG.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)

#
# Print and go!
#
pc.printRequestRSpec(request)
