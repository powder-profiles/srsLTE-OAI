# About This Profile


Use this profile to instantiate an experiment to realize an end-to-end SDR-based mobile network. These are the possible options for each component.  
UE : srsLTE UE or Nexus 5  
eNB : srsLTE eNB  
Core network (CN) : Open Air Interface core network or srsLTE core network  

You can select one of them, in the drop down menu "UE type",
while instantiating the experiment (i.e., in Parameterize step)
In `epc` node in your experiment, you can run either OAI CN or srsLTE CN.  

Be sure to setup your SSH keys as outlined in the manual; it's better to log in via a real SSH client to the nodes in your experiment.

# Getting Started with Nexus 5 UE

############################ Run OAI-CN ############################  
Access to `epc` node.

1. Run OAI-CN  
$ sudo /local/repository/bin/start_oai.pl

This will stop any currently running OAI services, start all services
(mme. sgw, and pge) again.
It saves logs to `/var/log/oai/*` on the `epc` nodes.

2. Check whether mme, hss, spgw run or not.  
$ sudo screen -list  
There are screens on:  
	16227.spgw	(06/12/2018 12:05:25 PM)	(Detached)  
	16130.mme	(06/12/2018 12:05:19 PM)	(Detached)  
	16053.hss	(06/12/2018 12:05:13 PM)	(Detached)  
3 Sockets in /var/run/screen/S-root.  


Note that ignore below message if you see below output after running start_oai.pl  

"ssh: Could not resolve hostname enb1: Name or service not known  
ERROR: Could not detect USRP B210 radio on the enb1 node. This is usually a transient error. Reboot the enb1 node and try again."  

############################ Run srsLTE eNB ############################  
Access to `srsenb` node.


1. Make cpu performance mode   
$ cd /local/repository/config/srsLTE  
$ sudo bash governor.sh  

2. Change enb configuration file (e.g., change rx_gain values. I tested 30 or 40).   
$ cd /usr/local/src/srsLTE/build/srsenb/src  
$ vim enb.conf  
tx_gain = 90  
rx_gain = 30  

3. Run srsLTE eNB  
$ cd /usr/local/src/srsLTE/build/srsenb/src  
$ sudo ./srsenb ./enb.conf  

############################ Run Nexus 5 UE ############################  
Access to `srsue` node.

$ pnadb -a  
$ adb shell  
$ ping 8.8.8.8  

When using real UE, to access the UE via ADB, first log in to the `adb-tgt`
node, then run `pnadb -a` to connect to the UE.  Use ADB commands as per
normal afterward.  If/when you reboot the UE, be aware that you will need
to again run `pnadb -a` to reestablish the ADB connection; wait a minute
or so for the UE to become available again before doing this.


# Getting Started with srsUE
Default configurations of UE subscription and Radio to use SDR-version UE. 

1. User subscription info  
imsi : 998981234560308  
algo : milenage  
op   : 01020304050607080910111213141516  
k    : 00112233445566778899aabbccddeeff  
amf  : 8000  

2. LTE band : 4  

############################ Run OAI-CN ############################  
Access to `epc` node.

1. Provision UE in OAI-CN HSS  
$ cd  /local/repository/config/oai-db  
$ mysql -u root -p # password : linux  
``` bash
mysql> use oai_db  
mysql> INSERT INTO pdn (`id`, `apn`, `pdn_type`, `pdn_ipv4`, `pdn_ipv6`, `aggregate_ambr_ul`, `aggregate_ambr_dl`, `pgw_id`, `users_imsi`, `qci`, `priority_level`,`pre_emp_cap`,`pre_emp_vul`, `LIPA-Permissions`) VALUES ('3',  'oai.ipv4','IPV4', '0.0.0.0', '0:0:0:0:0:0:0:0', '50000000', '100000000', '2',  '998981234560308', '9', '15', 'DISABLED', 'ENABLED', 'LIPA-ONLY');   
mysql> INSERT INTO users (`imsi`, `msisdn`, `imei`, `imei_sv`, `ms_ps_status`, `rau_tau_timer`, `ue_ambr_ul`, `ue_ambr_dl`, `access_restriction`, `mme_cap`, `mmeidentity_idmmeidentity`, `key`, `RFSP-Index`, `urrp_mme`, `sqn`, `rand`, `OPc`) VALUES ('998981234560308',  '33638060308', NULL, NULL, 'PURGED', '120', '50000000', '100000000', '47', '0000000000', '1', 0x00112233445566778899aabbccddeeff, '1', '0', 0, 0x00000000000000000000000000000000, 0x01020304050607080910111213141516);   
```

2. Run OAI-CN  
$ sudo /local/repository/bin/start_oai.pl  


3. Check whether mme, hss, spgw run or not.  
$ sudo screen -list  
There are screens on:  
	16227.spgw	(06/12/2018 12:05:25 PM)	(Detached)  
	16130.mme	(06/12/2018 12:05:19 PM)	(Detached)  
	16053.hss	(06/12/2018 12:05:13 PM)	(Detached)  
3 Sockets in /var/run/screen/S-root.  


Note that ignore below message if you see below output after running start_oai.pl  

"ssh: Could not resolve hostname enb1: Name or service not known  
ERROR: Could not detect USRP B210 radio on the enb1 node. This is usually a transient error. Reboot the enb1 node and try again."  


############################ Run srsLTE eNB ############################  
Access to `srsenb` node.  

1. Make cpu performance  
$ cd /local/repository/config/srsLTE  
$ sudo bash governor.sh  

2. Run srsLTE eNB  
$ cd /usr/local/src/srsLTE/build/srsenb/src  
$ sudo ./srsenb ./enb.conf  



############################ Run srsLTE UE ############################  
Access to `srsue` node.  
1. Make cpu performance  
$ cd /local/repository/config/srsLTE  
$ sudo bash governor.sh  

2. Run srsLTE UE  
$ cd /usr/local/src/srsLTE/build/srsue/src  
$ sudo ./srsue ./ue.conf 

If you see `Network attach successful. IP: 172.16.0.6` (note that you may get different IP address) output after running srsue, ue is correctly connected and this is ip address assigned for the ue.  

3. Sending traffic  
# It needs to set up routing tables to use LTE/EPC connectivity.  
# change mtu size - mandatory??  
$ sudo ip link set mtu 1400 dev tun_srsue  
# Set up routing table  
# You need to use right ip address for gw as your tun_srsue ip address.  
# In this example, ue gets 172.16.0.6 ip address from EPC  
$ sudo route add -net 8.8.8.8 netmask 255.255.255.255 gw 172.16.0.6 dev tun_srsue  
$ ping 8.8.8.8

Or you can use a script in /local/repository/config/srsLTE/srsue/ping-to-google.sh which is a script for above commands.   
$ sudo bash ping-to-google.sh 172.16.0.6  


# Getting Started with srsLTE core network
This section explains how to use srsLTE core network instead of OAI CN.  

############################ Run srsLTE EPC with srsLTE UE ############################  
Currently, a configuration file for UE information in srsLTE EPC has the same information as srsLTE UE.  


Access to `epc` node.  
1. Set up internet connectivity for UE traffic in `epc` node.
Note that you need to specify a network interface which has public IP address (e.g., 155.98.x.x) in your experiment when you run `if_masq.sh` script. In general, its name is `eno1` in your experiment. But you need to check it with `ifconfig` command.   
$ cd /usr/local/src/srsLTE/srsepc  
$ sudo ./if_masq.sh <Interface Name>

2. Run srsLTE EPC   
$ cd /usr/local/src/srsLTE/build/srsepc/src   
$ sudo ./srsepc ./epc.conf



############################ Run srsLTE EPC with Nexus 5 ############################  
If you use Nexus 5, you need to change `user_db.csv` file in /usr/local/src/srsLTE/build/srsepc/src directory.
Especially, you need to change the second column (i.e., IMSI information). 
To know IMSI of Nexus 5, please use `N5_SIM.sh` and use the output in `user_db.csv`.      


1. Access to `adb-tgt` node.   
$ cd /local/repository/config/srsLTE/srsepc  
$ bash ./N5_SIM.sh


For more detailed information:

  * [Controlling OAI](https://gitlab.flux.utah.edu/powder-profiles/OAI-Real-Hardware/blob/master/control.md)
  * [Inspecting OAI](https://gitlab.flux.utah.edu/powder-profiles/OAI-Real-Hardware/blob/master/inspect.md)
  * [Modifying OAI](https://gitlab.flux.utah.edu/powder-profiles/OAI-Real-Hardware/blob/master/modify.md)
  * [Modifying This Profile](https://gitlab.flux.utah.edu/powder-profiles/OAI-Real-Hardware/blob/master/modify-profile.md)
