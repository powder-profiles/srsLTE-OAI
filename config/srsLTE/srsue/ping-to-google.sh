#!/bin/bash

argc=$#

if [[ $argc -ne 1 ]]
then
	echo "[tun_srsue ip addr]"
	exit -1
fi

ip=$1

# change mtu size
sudo ip link set mtu 1400 dev tun_srsue
# To receive data from epc node
sudo route add -net 8.8.8.8 netmask 255.255.255.255 gw $ip dev tun_srsue
ping 8.8.8.8
